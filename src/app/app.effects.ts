import { Actions, Effect, ofType } from '@ngrx/effects';
import { map } from 'rxjs/operators';
import { of } from 'rxjs';
import { Injectable } from '@angular/core';
import * as actions from './app.reducer';

@Injectable()
export class AppEffects {
  @Effect()
  updateTextOnIncrement$ = this.actions$.pipe(
    ofType(actions.INCREMENT),
    map(() => ({ type: actions.DECREMENT }))
  );

  constructor(private actions$: Actions) {}
}
