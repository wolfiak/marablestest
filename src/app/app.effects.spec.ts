import { TestBed } from '@angular/core/testing';
import { provideMockActions } from '@ngrx/effects/testing';
import { AppEffects } from './app.effects';
import { Subject, Observable } from 'rxjs';
import { cold, hot } from 'jasmine-marbles';
import * as actionTypes from './app.reducer';
import {Actions} from '@ngrx/effects';

describe('App Effects', () => {
  let effects: AppEffects;
  let actions: Observable<any>;
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        // any modules needed
      ],
      providers: [
        AppEffects,
        provideMockActions(() => actions)
        // other providers
      ]
    });

    effects = TestBed.get(AppEffects);
  });

  it('should work', () => {
    actions = hot('--a', { a: { type: actionTypes.INCREMENT } });
    // const effectsT = new AppEffects(new Actions(source));
    const expected = cold('--b', { b: { type: actionTypes.DECREMENT } });

    expect(effects.updateTextOnIncrement$).toBeObservable(expected);
  });
});
